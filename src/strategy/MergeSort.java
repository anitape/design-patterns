package strategy;

public class MergeSort implements SortStrategy {
	private int[] tempMergArr;
	
	private void mergeSort(int arr[], int lowerIndex, int higherIndex) {

		if (lowerIndex < higherIndex) {
			int middle = lowerIndex + (higherIndex - lowerIndex) / 2;
			// Below step sorts the left side of the array
			mergeSort(arr, lowerIndex, middle);
			// Below step sorts the right side of the array
			mergeSort(arr, middle + 1, higherIndex);
			// Now merge both sides
			mergeParts(arr, lowerIndex, middle, higherIndex);
		}
	}

	private void mergeParts(int arr[], int lowerIndex, int middle, int higherIndex) {

		for (int i = lowerIndex; i <= higherIndex; i++) {
			tempMergArr[i] = arr[i];
		}
		int i = lowerIndex;
		int j = middle + 1;
		int k = lowerIndex;
		while (i <= middle && j <= higherIndex) {
			if (tempMergArr[i] <= tempMergArr[j]) {
				arr[k] = tempMergArr[i];
				i++;
			} else {
				arr[k] = tempMergArr[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			arr[k] = tempMergArr[i];
			k++;
			i++;
		}

	}

	@Override
	public void sort(int arr[]) {
		this.tempMergArr = new int[arr.length];
		mergeSort(arr, 0, arr.length - 1);
	}
}
