package strategy;

public class Main {

	public static void main(String[] args) {
		Context context;
		long startTime;
		long duration;
		int arrSize = 5000;

		int[] arr = createArray(arrSize);
		
		
		context = new Context(new SelectionSort());
		startTime = System.currentTimeMillis();
		context.implementStrategy(arr);
		duration = System.currentTimeMillis() - startTime;
		
		System.out.println("SelectionSort:");
		for (int i : arr) {
			System.out.print(i);
			System.out.print(", ");
		}

		System.out.println("\nDuration of selectionsort is: " + duration + " ms.");
		System.out.println("\n");

		context = new Context(new QuickSort());
		startTime = System.currentTimeMillis();
		context.implementStrategy(arr);
		duration = System.currentTimeMillis() - startTime;

		System.out.println("QuickSort:");
		for (int i : arr) {
			System.out.print(i);
			System.out.print(", ");
		}

		System.out.println("\nDuration of quicksort is: " + duration + " ms.");
		System.out.println("\n");

		context = new Context(new MergeSort());
		startTime = System.currentTimeMillis();
		context.implementStrategy(arr);
		duration = System.currentTimeMillis() - startTime;

		System.out.println("MergeSort:");
		for (int i : arr) {
			System.out.print(i);
			System.out.print(", ");
		}
		System.out.println("\nDuration of mergesort is: " + duration + " ms.");
	}

	public static int[] createArray(int size) {
		int[] arr = new int[size];
		int min = 1;
		int max = 10000;

		for (int i = 0; i < size; i++) {
			arr[i] = min + (int) (Math.random() * ((max - min) + 1));
		}

		return arr;
	}
}