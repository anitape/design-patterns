package strategy;

public class Context {

	private SortStrategy strategy;
	
	public Context(SortStrategy strategy) {
		this.strategy=strategy;
	}
	
	public void implementStrategy(int arr[]) {
		this.strategy.sort(arr);
	}
	
}
