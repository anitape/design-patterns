package iterator;

import java.util.Iterator;

public class MyThread extends Thread {
	String name;
	Iterator<Integer> i;
	Iterator<Integer> itr;
	Synchronized sync;

	public MyThread(String name, Iterator<Integer> i, Synchronized sync) {
		this.name = name;
		this.i = i;
		this.sync = sync;
	}

	public void run() {
		sync.iterate(name, i);
		// i.remove(); //it's possible to remove iterator
	}
}
