package iterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class Main {
	public static void main(String[] args) {
		Collection<Integer> c = new ArrayList<>(); //ArrayList<>();
		
		c.add(1);
		c.add(2);
		c.add(3);
		c.add(4);
		
		Iterator<Integer> i = c.iterator();
		Iterator<Integer> i2 = c.iterator();
		
		Synchronized sync = new Synchronized();
		
		MyThread threadOne = new MyThread("Thread 1", i, sync);
		MyThread threadTwo = new MyThread("Thread 2", i, sync); //each number occurs just once
	    //MyThread threadTwo = new MyThread("Thread 2", i2, sync); // both iterators' numbers occur in turn in ascending order
	    
	    threadOne.start();
	   // c.add(8); // error
	    threadTwo.start();
	}
}
