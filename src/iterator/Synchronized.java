package iterator;

import java.util.Iterator;

public class Synchronized {	
	public synchronized void iterate(String name, Iterator<Integer> itr) {
		while (itr.hasNext()) {
			System.out.println(name + ": " + itr.next());
			try {
				wait(100);
				notifyAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
