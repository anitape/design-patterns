package visitor;

public abstract class PokemonState {
	
	public void speak(PokemonContext pokemon) {}
	public void fight(PokemonContext pokemon) {}
	public void run(PokemonContext pokemon) {}
	public void progress(PokemonContext pokemon) {}
	public void setState(PokemonContext pokemon, PokemonState state) {
		pokemon.setState(state);
	}

    public void accept(Visitor visitor) {}

}
