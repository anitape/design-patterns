package visitor;

public class lvl2Pikachu extends PokemonState {
	
	@Override
    public void speak(PokemonContext context) {
        System.out.println("Pika pika.");
    }
    
    @Override
    public void fight(PokemonContext context) {
        System.out.println("I hit my enemy with Thunder Shock of 5 000 V.");        
    }
    
    @Override
    public void run(PokemonContext context) {
    	System.out.println("I'm running away with the speed of Electricity (2500km/s).");
    }
    
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    
    @Override
    public void progress(PokemonContext context) {
    	context.speak();
    	context.fight();
    	context.run();
    	context.accept(new BonusVisitor());
    	context.setState(new lvl3Raichu());
    }
}
