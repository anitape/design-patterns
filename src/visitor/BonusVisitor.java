package visitor;

public class BonusVisitor implements Visitor {

	@Override
	public void visit(lvl1Pichu pichu) {
		System.out.println("\n+10 bonus points earned.\n");
	}

	@Override
	public void visit(lvl2Pikachu pikachu) {
		System.out.println("\n+1 000 bonus points earned.\n");
	}

	@Override
	public void visit(lvl3Raichu raichu) {
		System.out.println("\n+10 000 bonus points earned.\n");
	}

}
