package visitor;

public class lvl1Pichu extends PokemonState {
			
	 	@Override
	    public void speak(PokemonContext context) {
	        System.out.println("Pichu pichu.");
	    }
	    
	    @Override
	    public void fight(PokemonContext context) {
	        System.out.println("I hit my enemy with Electric Shock of 100 V.");        
	    }
	    
	    @Override
	    public void run(PokemonContext context) {
	    	System.out.println("I'm running away with the speed of Tornado (1000km/h).");
	    }
	    
	    @Override
	    public void accept(Visitor visitor) {
	        visitor.visit(this);	    	
	    }
	    
	    @Override
	    public void progress(PokemonContext context) {
	    	context.speak();
	    	context.fight();
	    	context.run();
	    	context.accept(new BonusVisitor());
	    	context.setState(new lvl2Pikachu());
	    }
	        
}
