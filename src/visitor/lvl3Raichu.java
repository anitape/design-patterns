package visitor;

public class lvl3Raichu extends PokemonState {

	@Override
	public void speak(PokemonContext context) {
		System.out.println("Raichu raichu.");
	}

	@Override
	public void fight(PokemonContext context) {
		System.out.println("I hit my enemy with Lightning Bolt of 1 000 000 V.");
	}

	@Override
	public void run(PokemonContext context) {
		System.out.println("I'm running away with the speed of Lightning (300 000km/s).");
	}

	@Override
	public void accept(Visitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void progress(PokemonContext context) {
		context.speak();
		context.fight();
		context.run();
		context.accept(new BonusVisitor());
	}
}
