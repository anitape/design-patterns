package visitor;

public interface Visitor {
	public void visit(lvl1Pichu pichu);
	public void visit(lvl2Pikachu pikachu);
	public void visit(lvl3Raichu raichu);
}
