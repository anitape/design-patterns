package visitor;

public class PokemonContext {

	private PokemonState lvlState;

	public PokemonContext() {
		lvlState = new lvl1Pichu();
	}	

	public void speak() {
		lvlState.speak(this);
	}

	public void fight() {
		lvlState.fight(this);
	}

	public void run() {
		lvlState.run(this);
	}
	
	public void setState(PokemonState state) {
		lvlState = state;
	}
	
	public void accept(Visitor visitor) {
		lvlState.accept(visitor);
	}

	public void progress() {
		lvlState.progress(this);
	}
	
}
