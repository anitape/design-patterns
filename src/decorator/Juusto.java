package decorator;

public class Juusto extends PizzaDecorator {
	
	public Juusto(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Juusto";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 1.0;
	}
}
