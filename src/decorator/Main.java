package decorator;

import java.util.ArrayList;

public class Main {
    
    public static void main(String[] args) {
    	
    	ArrayList<Pizza> pizzamenu = new ArrayList<>();
    	pizzamenu.add(new Punasipuli (new Ananas (new Herkkusieni (new Juusto ( new Tomaatti (new PizzaPohja (new SimplePizza())))))));
    	pizzamenu.add(new Oliivi (new Paprika (new Katkarapu (new Juusto ( new Tomaatti (new PizzaPohja (new SimplePizza())))))));
    	pizzamenu.add(new Paprika (new Ananas (new Punasipuli (new Herkkusieni (new Katkarapu (new Juusto ( new Tomaatti (new PizzaPohja (new SimplePizza())))))))));
    	
    	System.out.println("PIZZAMENU \n");
    			
    	for (Pizza pizza: pizzamenu) {
    		System.out.println("Pizzaan sisältyy: " + pizza.getAines() + " " + pizza.getHinta() + " €.");
    	}
    }
}