package decorator;

public class PizzaDecorator implements Pizza {
	
	protected Pizza tayte;
	
	public PizzaDecorator(Pizza pizzaaines) {
		this.tayte = pizzaaines;
	}
	
	@Override
	public String getAines() {
		return tayte.getAines();
	}
	
	@Override
	public double getHinta() {
		return tayte.getHinta();
	}
	
	@Override
	public String toString() {
		return "Pizzatayte " + tayte.getAines() +
				" hinta " + tayte.getHinta();
	}
	
}
