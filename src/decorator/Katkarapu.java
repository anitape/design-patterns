package decorator;

public class Katkarapu extends PizzaDecorator{
	
	public Katkarapu(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Katkarapu";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 1.5;
	}
}
