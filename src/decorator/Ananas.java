package decorator;

public class Ananas extends PizzaDecorator {
	
	public Ananas(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Ananas";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 0.5;
	}
}
