package decorator;

public class Tomaatti extends PizzaDecorator {
	
	public Tomaatti(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Tomaatti";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 0.5;
	}

}
