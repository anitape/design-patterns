package decorator;

public class Herkkusieni extends PizzaDecorator {
	
	public Herkkusieni(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Herkkusieni";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 1.0;
	}
}
