package decorator;

public class Punasipuli extends PizzaDecorator{
	
	public Punasipuli(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Punasipuli";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 0.5;
	}
}
