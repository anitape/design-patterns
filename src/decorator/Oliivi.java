package decorator;

public class Oliivi extends PizzaDecorator {
	
	public Oliivi(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Oliivi";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 0.5;
	}
}
