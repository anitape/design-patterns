package decorator;

public class PizzaPohja extends PizzaDecorator {
	
	public PizzaPohja(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Pizzapohja";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 1.0;
	}
}
