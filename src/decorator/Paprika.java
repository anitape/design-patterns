package decorator;

public class Paprika extends PizzaDecorator {
	
	public Paprika(Pizza pizzaaines) {
		super(pizzaaines);
	}
	
	@Override
	public String getAines() {
		return super.getAines() + " Paprika";
	}
	
	@Override
	public double getHinta() {
		return super.getHinta() + 0.5;
	}
}
