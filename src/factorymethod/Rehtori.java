package factorymethod;

public class Rehtori extends AterioivaOtus {
	
	public Juoma createJuoma(){
        return new Tee();
    };
    
    public Ruoka createRuoka() {
    	return new Pihvi();
    };
}
