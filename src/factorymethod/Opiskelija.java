package factorymethod;

public class Opiskelija extends AterioivaOtus {
	
	public Juoma createJuoma(){
        return new Kahvi();
    };
    
    public Ruoka createRuoka() {
    	return new Keitto();
    };
}
