package facade;

/* Complex parts */
public class Memory {
	public void load(long position, char[] data) {
		System.out.println("Loading from: - " + position + "...");
		System.out.println("Data is");
		for (int i = 0; i < data.length; i++) {
			System.out.println(data[i]);
		}
	}
}
