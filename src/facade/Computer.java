package facade;

/* Facade */
public class Computer {
	private CPU cpu;
	private Memory memory;
	private HardDrive hardDrive;
	
	private long BOOT_ADDRESS = 2031;
	private long BOOT_SECTOR = 20;
	private int SECTOR_SIZE = 512;


	public Computer() {
		this.cpu = new CPU();
		this.memory = new Memory();
		this.hardDrive = new HardDrive();
	}

	public void startComputer() {
		cpu.freeze();
		memory.load(BOOT_ADDRESS, hardDrive.read(BOOT_SECTOR, SECTOR_SIZE));
		cpu.jump(BOOT_ADDRESS);
		cpu.execute();
	}
}
