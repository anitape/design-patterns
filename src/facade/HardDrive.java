package facade;

import java.util.Random;

/* Complex parts */
public class HardDrive {
	
	public char[] read(long lba, int size) {
		char[] byteData = new char[size];
		for (int i = 0; i < byteData.length; i++) {
			Random r = new Random();
			char c = (char)(r.nextInt(25) + 'a');
			byteData[i] = c;
		}
		
		System.out.println("HardDrive loaded data from: - " + lba + " -");
		return byteData;
	}
}
