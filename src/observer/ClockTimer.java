package observer;
import java.util.Observable;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ClockTimer extends Observable implements Runnable {
	
	//muuttujat
	private int hour;
	private int minute;
	private int second;
	
	public ClockTimer() {
		hour = 0;
		minute = 0;
		second = 0;			
	}
	
	public ClockTimer(int hour, int minute, int second) {
		this.hour= hour;
		this.minute = minute;
		this.second = second;
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public int getSecond() {
		return second;
	}
	
	void tick() {
		second++;
		if (second > 59) {
			second = 0;
			minute++;
		}
		
		if (minute > 59) {
			minute = 0;
			hour++;
		}
		
		this.setChanged();
		this.notifyObservers();
	}
	
	@Override
	public void run() {
		while (hour < 1) {
			tick();
			try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClockTimer.class.getName()).log(Level.SEVERE, null, ex);
            }

		}
	}
	
}