package chainofresponsibility;

public class Supervisor extends Salary {
	private double allowed = 1.02 * salary;

	public void processRequest(SalaryRequest request) {
		if (request.getSum() <= salary) {
			System.out.println("Supervisor cannot lower your salary");
		}

		else if (request.getSum() <= allowed) {
			System.out.println();
			System.out.println("Supervisor will handle the request");
			System.out.println("Your new salary is " + request.getSum());
			System.out.println();

		}

		else if (successor != null) {
			successor.processRequest(request);

		}

	}
}
