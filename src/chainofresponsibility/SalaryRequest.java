package chainofresponsibility;

public class SalaryRequest {

	private double sum;

	public SalaryRequest(double sum){
        this.sum = sum;
    }

	public void setSum(double sum) {
		this.sum = sum;
	}

	public double getSum() {
		return sum;
	}

}
