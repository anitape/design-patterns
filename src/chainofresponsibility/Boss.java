package chainofresponsibility;

public class Boss extends Salary {
	
	private double min = 1.02 * salary;
    private double max = 1.05 * salary;

    public void processRequest(SalaryRequest request) {
        if (request.getSum() <= max && request.getSum() > min) { // lower than max, higher than min
            System.out.println();
            System.out.println("Boss will handle the request");
            System.out.println("Your new salary is " + request.getSum());
            System.out.println();
        
        }
        
        else if (successor != null) {
            successor.processRequest(request);

        }
        
    }
    

}
