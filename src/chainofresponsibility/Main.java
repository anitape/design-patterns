package chainofresponsibility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) {
		Supervisor supervisor = new Supervisor(); // 1
		Boss boss = new Boss(); // 2
		Executive executive = new Executive(); // 3

		supervisor.setSuccessor(boss); // Successor
		boss.setSuccessor(executive); // Successor

		System.out.println("Your salary is " + Salary.salary + "\n");

		try {
			while (true) {
				System.out.println("Your raise request?");
				double request = Double.parseDouble(new BufferedReader(new InputStreamReader(System.in)).readLine());
				supervisor.processRequest(new SalaryRequest(request));

			}

		} catch (Exception e) {
			System.exit(1);
		}

	}

}
