package chainofresponsibility;

public class Executive extends Salary {
	private double allowed = 1.05 * salary;

	public void processRequest(SalaryRequest request) {
		if (request.getSum() > salary) {
			System.out.println();
			System.out.println("Executive will handle the request");
			System.out.println("Your new salary is " + request.getSum());
			System.out.println();

		}

		else if (successor != null) {
			successor.processRequest(request);
			System.out.println("Nobody handled your request...");

		}

	}

}
