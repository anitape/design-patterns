package chainofresponsibility;

public abstract class Salary {
	
	protected static double salary = 3000;
    protected Salary successor;
    
    public void setSuccessor(Salary successor){
        this.successor = successor;
        
    }
    
    abstract public void processRequest(SalaryRequest request);

}
