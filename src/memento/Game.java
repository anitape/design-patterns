package memento;

public class Game {

	private int random;

	public Object joinGame(Client client) {
		random = (int) (Math.random() * 10) + 1;
		return new Memento(random);
	}

	public boolean handleGuess(Object obj, int guess) {
		Memento memento = (Memento) obj;
		System.out.println("Game's number is " + memento.number);
		if (guess == memento.number) {
			return true;
		} else {
			return false;
		}

	}

	private class Memento {

		private int number;

		public Memento(int num) {
			this.number = new Integer(num);
		}

	}
}
