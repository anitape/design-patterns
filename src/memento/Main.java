package memento;

public class Main {

	public static void main(String[] args) {

		Game game = new Game();
		
		Client c1 = new Client(game, "Client 1");
		c1.start();
		
		Client c2 = new Client(game, "Client 2");
		c2.start();
		/*
		Client c3 = new Client(game, "Client 3");
		c3.start();*/
		
	}
}
