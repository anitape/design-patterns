package memento;

public class Client extends Thread {

	private Object object;
	private Game game;
	private String name;
	private boolean rand = false;
	private int guessed;

	public Client(Game game, String name) {
		this.game = game;
		this.name = name;
	}

	//@Override
	public void run() {
		this.object = game.joinGame(this);
		while (!rand) {
			guessed = (int) (Math.random() * 10) + 1;
			rand = game.handleGuess(this.object, guessed);
			if (!rand) {
			System.out.println(name + " guessed number " + guessed);	
			}
		}
		System.out.println(name + " guessed number " + guessed);
		System.out.println(name + " GUESSED THE NUMBER.");
		
	}

}
