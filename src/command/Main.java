package command;

public class Main {
	public static void main(String[] args) {
		Light lamp = new Light();
		Screen screen = new Screen();
		Command switchUp = new FlipUpCommand(lamp);
		Command switchDown = new FlipDownCommand(lamp);
		Command upScreen = new ScreenUpCommand(screen);
		Command downScreen = new ScreenDownCommand(screen);

		WallButton button1 = new WallButton(switchUp);
		WallButton button2 = new WallButton(switchDown);
		WallButton button3 = new WallButton(upScreen);
		WallButton button4 = new WallButton(downScreen);

		button1.push();
		button2.push();

		button3.push();
		button4.push();

	}
}
