package command;

public class Screen {
	public void moveUp() {
		System.out.println("Screen is moving up");
	}

	public void moveDown() {
		System.out.println("Screen is moving down");
	}
}
