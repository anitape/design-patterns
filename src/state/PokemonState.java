package state;

public interface PokemonState {
	
	public void speak();
	public void fight();
	public void run();
}
