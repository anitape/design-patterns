package state;

public class PokemonContext implements PokemonState {
	
	private PokemonState lvlState;
	
	public void setState(PokemonState state){
	     this.lvlState = state;
	}  

	public PokemonState getState(){
	     return this.lvlState;
	}
	    	
    @Override
    public void speak() {
        this.lvlState.speak();
    }

    @Override
    public void fight() {
        this.lvlState.fight();
    }
    
    @Override
    public void run() {
        this.lvlState.run();
    }

}
