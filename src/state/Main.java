package state;

public class Main {


    public static void main(String[] args) {

        PokemonContext context = new  PokemonContext();
        PokemonState lvl1 = new lvl1Pichu();
        PokemonState lvl2 = new lvl2Pikachu();
        PokemonState lvl3 = new lvl3Raichu();
        
        context.setState(lvl1);
        context.speak();
        context.fight();
        context.run();
        
        context.setState(lvl2);
        context.speak();
        context.fight();
        context.run();
        
        context.setState(lvl3);
        context.speak();
        context.fight();
        context.run();
    }
}
