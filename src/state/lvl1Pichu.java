package state;

public class lvl1Pichu implements PokemonState {
		
	 	@Override
	    public void speak() {
	        System.out.println("Pichu pichu.");
	    }
	    
	    @Override
	    public void fight() {
	        System.out.println("I hit my enemy with Electric Shock of 100 V.");        
	    }
	    
	    @Override
	    public void run() {
	    	System.out.println("I'm running away with the speed of Tornado (1000km/h).\n");
	    }
	    
}
