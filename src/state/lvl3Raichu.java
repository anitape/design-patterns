package state;

public class lvl3Raichu implements PokemonState {
	
	@Override
    public void speak() {
        System.out.println("Raichu raichu.");
    }
    
    @Override
    public void fight() {
        System.out.println("I hit my enemy with Lightning Bolt of 1 000 000 V.");        
    }
    
    @Override
    public void run() {
    	System.out.println("I'm running away with the speed of Lightning (300 000km/s).\n");
    }
    
}
