package state;

public class lvl2Pikachu implements PokemonState {
 	
	@Override
    public void speak() {
        System.out.println("Pika pika.");
    }
    
    @Override
    public void fight() {
        System.out.println("I hit my enemy with Thunder Shock of 5 000 V.");        
    }
    
    @Override
    public void run() {
    	System.out.println("I'm running away with the speed of Electricity (2500km/s).\n");
    }
}
