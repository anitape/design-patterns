package adapter;

public class Main {
	public static void main(String[] args) {
		MediaPlayer player = new MP3();
	    player.play("file.mp3");
	    
	    System.out.print("\nSelecting file.mp4...");
	    player = new MP4(new FormatAdapter());
	    player.play("file.mp4");
	    
	    System.out.print("\nSelecting file.avi...");
	    player = new VLC(new FormatAdapter());
	    player.play("file.avi");
	}
}
