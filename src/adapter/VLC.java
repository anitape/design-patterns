package adapter;

public class VLC extends FormatAdapter implements MediaPlayer {
	private FormatAdapter adapter;
	
	public VLC(FormatAdapter fa) {
		adapter=fa;
	}
	
	@Override
	public void play(String filename) {
		adapter.changeFormat("VLC");
		System.out.println("Playing VLC File " + filename);
	}
}
