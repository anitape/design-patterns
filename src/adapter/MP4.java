package adapter;

public class MP4 extends FormatAdapter implements MediaPlayer {
	
	private FormatAdapter adapter;
	
	public MP4(FormatAdapter fa) {
		adapter=fa;
	}
	
	@Override
	public void play(String filename) {
		adapter.changeFormat("mp4");
		System.out.println("Playing MP4 File " + filename);
	}
}
