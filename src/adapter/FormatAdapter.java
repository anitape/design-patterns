package adapter;

public class FormatAdapter {

	public void changeFormat(String type) {
		System.out.println("File is in " + type + " format.");
		System.out.println("Using Adapter to switch to another mediaplayer.");
	}
}