package abstractfactory;

public class Jasper {
	
	public Farmarit pueFarmarit(PukeutumisTehdas tehdas) {
		Farmarit farmarit = tehdas.luoFarmarit();
		return farmarit;
	}
	
	public Paita puePaita(PukeutumisTehdas tehdas) {
		Paita paita = tehdas.luoPaita();
		return paita;
	}
	
	public Lippis pueLippis(PukeutumisTehdas tehdas) {
		Lippis lippis = tehdas.luoLippis();
		return lippis;
	}
	
	public Kengat pueKengat(PukeutumisTehdas tehdas) {
		Kengat kengat = tehdas.luoKengat();
		return kengat;
	}
}
