package abstractfactory;

public class AdidasTehdas implements PukeutumisTehdas{
	
	public Farmarit luoFarmarit() {			
		return new AdidasFarmarit();
	}
	
	public Paita luoPaita() {
		return new AdidasPaita();
	}
	
	public Lippis luoLippis() {
		return new AdidasLippis();
	}
	
	public Kengat luoKengat() {			
		return new AdidasKengat();
	}
}
