package abstractfactory;

public class Main {
	
	 public static void main(String[] args) {
		 Jasper hk = new Jasper();
		 Farmarit farmarit = hk.pueFarmarit(new AdidasTehdas());
		 Paita paita = hk.puePaita(new AdidasTehdas());
		 Lippis lippis = hk.pueLippis(new AdidasTehdas());
		 Kengat kengat = hk.pueKengat(new AdidasTehdas());
		 
		 Farmarit farmarit1 = hk.pueFarmarit(new BossTehdas());
		 Paita paita1 = hk.puePaita(new BossTehdas());
		 Lippis lippis1 = hk.pueLippis(new BossTehdas());
		 Kengat kengat1 = hk.pueKengat(new BossTehdas());
		 
		 System.out.println("Opiskelija-Jasperilla on päällä: "+ farmarit + ", " + paita + ", " + lippis + " ja " + kengat + ".");
		 
		 System.out.println("Valmistuneella Jasperilla on päällä: "+ farmarit1 + ", " + paita1 + ", " + lippis1 + " ja " + kengat1 + ".");
	
	 }
}
