package abstractfactory;

public interface PukeutumisTehdas {
	
	public abstract Farmarit luoFarmarit();
	public abstract Paita luoPaita();
	public abstract Lippis luoLippis();
	public abstract Kengat luoKengat();
	
}