package abstractfactory;

public class BossTehdas implements PukeutumisTehdas {
	
	public Farmarit luoFarmarit() {		
		return new BossFarmarit();
	}
	
	public Paita luoPaita() {			
		return new BossPaita();
	}
	
	public Lippis luoLippis() {			
		return new BossLippis();
	}
	
	public Kengat luoKengat() {			
		return new BossKengat();
	}
			
}
