package builder;

public class Hesburger {
	
	private StringBuilder heseburger = new StringBuilder();
	
	public void setBun() {
		this.heseburger.append("Fresh Oat Bun\n");
	}
	public void setSauce() {
		this.heseburger.append("Ketchup\n");
	}
	public void setCheese() {
		this.heseburger.append("Creamy Cheese\n");
	}
	public void setSteak() {
		this.heseburger.append("Beef steak\n");
	}
	public void setVegetables() {
		this.heseburger.append("Pickled cucumber\n");
	}
	public void setLettuce() {
		this.heseburger.append("Boston Lettuce\n");
	}
	
	public StringBuilder printBurger() {
		return heseburger;
	}
}
