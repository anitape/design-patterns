package builder;

public class Burger {
	
	private String bun = "";
	private String sauce = "";
	private String cheese = "";
	private String steak = "";
	private String vegetables = "";
	private String lettuce = "";
	
	public void setBun(String bun) {
		this.bun = bun;
	}
	public void setSauce(String sauce) {
		this.sauce=sauce;
	}
	public void setCheese(String cheese) {
		this.cheese =cheese;
	}
	public void setSteak(String steak) {
		this.steak = steak;
	}
	public void setVegetables(String vegetables) {
		this.vegetables = vegetables;
	}
	public void setLettuce(String lettuce) {
		this.lettuce = lettuce;
	}

}
