package builder;

import java.util.ArrayList;

public class McDonalds {
	
	private ArrayList<Object> macburger = new ArrayList<Object>();
	
	public void setBun() {
		this.macburger.add(new SesamBun());
	}
	
	public void setSauce() {
		this.macburger.add(new Mayonnaise());
	}
	
	public void setCheese() {
		this.macburger.add(new Chedar());
	}
	
	public void setSteak() {
		this.macburger.add(new ChickenSteak());
	}
	
	public void setVegetables() {
		this.macburger.add(new Tomato());
	}
	
	public void setLettuce() {
		this.macburger.add(new Iceberg());
	}
	
	public void printBurger() {
		for(Object mb : macburger) {
			System.out.println(mb);
		}
	}
}
