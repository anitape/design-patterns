package builder;

public class Director {
	private BurgerBuilder burgerBuilder;
	
	public void setBurgerBuilder(BurgerBuilder bb) {
		this.burgerBuilder = bb;
	}	
	
	public void constructBurger() {
		burgerBuilder.addBun();
		burgerBuilder.addSauce();
		burgerBuilder.addCheese();
		burgerBuilder.addSteak();
		burgerBuilder.addVegetables();
		burgerBuilder.addLettuce();
	}
}
