package builder;

public abstract class BurgerBuilder {

	
	public abstract void getBurger();	
	public abstract void addBun();
	public abstract void addSauce();
	public abstract void addCheese();
	public abstract void addSteak();
	public abstract void addVegetables();
	public abstract void addLettuce();

}
