package builder;

public class HesburgerBuilder extends BurgerBuilder{
	
	public Hesburger heseburger = new Hesburger();
	
	public void addBun() {
		heseburger.setBun();
	}
	public void addSauce() {
		heseburger.setSauce();
	}
	public void addCheese() {
		heseburger.setCheese();
		
	}
	public void addSteak() {
		heseburger.setSteak();
		
	}
	public void addVegetables() {
		heseburger.setVegetables();
	}
	public void addLettuce() {
		heseburger.setLettuce();
	}
	
	public void getBurger() {
		System.out.println("Hesburger burger:");
		System.out.println(heseburger.printBurger().toString());
	}	
}
