package builder;

public class Main {
	public static void main(String[] args) {
		Director director = new Director();
		
		BurgerBuilder hesburger = new HesburgerBuilder();	
		director.setBurgerBuilder(hesburger);
		director.constructBurger();		
		hesburger.getBurger();
		
		BurgerBuilder macburger = new McDonaldsBuilder();
		director.setBurgerBuilder(macburger);
		director.constructBurger();		
		macburger.getBurger();
	}

}
