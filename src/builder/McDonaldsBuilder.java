package builder;

public class McDonaldsBuilder extends BurgerBuilder {
	
public McDonalds mcdonalds = new McDonalds();
	
	public void addBun() {
		mcdonalds.setBun();
	}
	
	public void addSauce() {
		mcdonalds.setSauce();
	}
	
	public void addCheese() {
		mcdonalds.setCheese();		
	}
	
	public void addSteak() {
		mcdonalds.setSteak();
		
	}
	
	public void addVegetables() {
		mcdonalds.setVegetables();
	}
	
	public void addLettuce() {
		mcdonalds.setLettuce();
	}
	
	public void getBurger() {
		System.out.println("McDonalds burger:");
		mcdonalds.printBurger();
	}	
}
