package prototype;

public class Main {
	public static void main(String[] args) {
		Clock clock = new Clock(23,56);

		HourPointer hpointer = clock.getHourPointer();
		MinutePointer mpointer = clock.getMinutePointer();
		
		Clock clock2 = new Clock(21,48);
		HourPointer hpointer2 = clock2.getHourPointer();
		MinutePointer mpointer2 = clock2.getMinutePointer();

		for (int i = 0; i < 20; i++) {
			clock.tickTime();
			clock2.tickTime();
			System.out.println("Time1 is " + hpointer.getHours() + ":" + mpointer.getMinutes() + ", Time2 is " + hpointer2.getHours() + ":" + mpointer2.getMinutes());
		}
	}
}
