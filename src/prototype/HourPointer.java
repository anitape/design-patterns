package prototype;

public class HourPointer implements Cloneable {
	private int hours;

	public HourPointer(int hours) {
		this.hours = hours;
	}

	public int getHours() {
		return hours;
	}

	public void tickHours() {
		if (this.hours == 23) {
			this.hours = 0;
		} else {
			this.hours++;
		}
	}

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

}
