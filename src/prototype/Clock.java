package prototype;

public class Clock implements Cloneable {
	private HourPointer hpointer;
	private MinutePointer mpointer;

	public Clock(int hour, int min) {
		this.hpointer = new HourPointer(hour);
		this.mpointer = new MinutePointer(min);
	}

	public HourPointer getHourPointer() {
		return hpointer;
	}
	
	public MinutePointer getMinutePointer() {
		return mpointer;
	}

	public Clock clone() {
		Clock clock = null;
		try {
			clock = (Clock) super.clone();
			clock.hpointer = (HourPointer) super.clone();
			clock.mpointer = (MinutePointer) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return clock;
	}
	
	public void tickTime() {
		mpointer.tickMinute();
        
        if(mpointer.getMinutes() == 0){
        hpointer.tickHours();
        }
        
	}

}
