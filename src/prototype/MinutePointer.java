package prototype;

public class MinutePointer {
	private int minutes;
	
	public MinutePointer(int min) {
		this.minutes = min;
	}
	
	public int getMinutes() {
		return minutes;
	}

	public void tickMinute() {
		if (this.minutes == 59) {
			this.minutes = 0;
		} else {
			this.minutes++;
		}
	}

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return null;
	}

}
