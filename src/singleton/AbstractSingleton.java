package singleton;

public interface AbstractSingleton {
	public void generate();
	public void name(String userName);

}
