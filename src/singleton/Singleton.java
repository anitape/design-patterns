package singleton;

public enum Singleton implements AbstractSingleton{
	INSTANCE;
	
	private String name;
	private int number;
	
	@Override
	public void name(String userName) {
		name = userName;
	}
	
	@Override
	public void generate() {
		number = 1 + (int)(Math.random() * 10);
		System.out.println(name + ", your lucky number is " + number + ".");
	}
	
}
