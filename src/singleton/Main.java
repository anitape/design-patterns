package singleton;

public class Main {
	public static void main(String[] args) {
		Singleton test = Singleton.INSTANCE;
		test.name("Bob");
		test.generate();
		
		Singleton test2 = Singleton.INSTANCE;
		test2.name("Marta");
		test2.generate();
	}
}
