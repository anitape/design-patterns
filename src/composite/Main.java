package composite;

public class Main {

	public static void main(String[] args) {
		
		double summa;
		
		Komponentti komponentti1 = new Komponentti("Muistipiiri", 40.99);
		Komponentti komponentti2 = new Komponentti("Prosessori", 50.99);
		Koostekomponentti koostekomponentti = new Koostekomponentti("Emolevy", 6.99);
		
		Komponentti komponentti3 = new Komponentti("Näytönohjain", 10.99);
		Komponentti komponentti4 = new Komponentti("Äänikortti", 14.99);
		
		koostekomponentti.lisaa(komponentti3);
		koostekomponentti.lisaa(komponentti4);
		
		summa = komponentti1.getHinta()+komponentti2.getHinta()+koostekomponentti.getHinta()+komponentti3.getHinta()+komponentti4.getHinta();
		
		System.out.println("Tietokone koostuu: \n");
		komponentti1.print();
		komponentti2.print();
		koostekomponentti.print();
		System.out.println("------------------------");
		System.out.println("Tietokoneen yhteishinta on  " + summa + " €");
	}
	
}
