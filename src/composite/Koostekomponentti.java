package composite;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class Koostekomponentti implements Laiteosa {
	
	private String nimi;
	private double hinta;
	
	public Koostekomponentti(String nimi, double hinta) {
		this.nimi=nimi;
		this.hinta=hinta;
	}
	
	public String getNimi() {
		return nimi;
	}
	
	public double getHinta() {
		return hinta;
	}
	
	private List<Laiteosa> laiteosat = new ArrayList <Laiteosa>();
	
	public void lisaa(Laiteosa laiteosa) {
		laiteosat.add(laiteosa);
	}
	
	public void print() {
	System.out.println(getNimi() + " - hinta " + getHinta() + " €");
	System.out.println(getNimi() + "-osaan liitetään seuraavat laiteosat: ");
	Iterator<Laiteosa> it = laiteosat.iterator();  
    
	    while(it.hasNext())  {  
	      Laiteosa laiteosa = it.next();  
	      laiteosa.print();  
	   } 
	}
}
