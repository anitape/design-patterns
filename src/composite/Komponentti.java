package composite;

public class Komponentti implements Laiteosa {
	
	private String nimi;
	private double hinta;
	
	public Komponentti(String nimi, double hinta) {
		this.nimi = nimi;
		this.hinta = hinta;
	}
	

	public String getNimi() {
		return nimi;
	}
	
	public double getHinta() {
		return hinta;
	}
	
	
	public void print() {
		System.out.println(getNimi() + " - hinta " + getHinta() + " €");
	}
	
	public void lisaa(Laiteosa laiteosa) {

	}
	
	
	
}
