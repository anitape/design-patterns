package templatemethod;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class CoinGame extends Game {
	
	Scanner sc = new Scanner(System.in);
	private int playerCoin;
	private int computerCoin;
	boolean endOfGame = true;
	
	@Override
    void initializeGame() {
		this.playerCoin = 0;
        this.computerCoin = 0;
	}
		
	@Override
    void makePlay(int player) {
		System.out.println("Welcome, this is a Coin Game! \n");
		System.out.println("You are a player number " + player + "\n");
		String computer;
        String user;
        
        // The computer's coin
		computer = computerCoin();

        // Get the user's coin choice.
        user = userCoin();

        // Tell the result of the game.
        String output = printCoins(computer, user);
        System.out.println(output);
	}

	@Override
	boolean endOfGame() {        
	    return endOfGame;
	}
	
	@Override
	void printWinner() {
		 if (playerCoin == computerCoin) {
	        System.out.println("YOU WIN.\n");
		 	//System.out.println("Playercoin is " + playerCoin);
		 	//System.out.println("Computercoin is " + computerCoin);
	}
		 else {
			System.out.println("THE COMPUTER WINS.\n");
			//System.out.println("Playercoin is " + playerCoin);
		 	//System.out.println("Computercoin is " + computerCoin);
		 }
	       playAgain();
	 }
	
	public String getCoin (int number) {

	    String choice;

	    switch (number) {
	        case 1:
	            choice = "Kruunu";
	            break;
	        case 2:
	            choice = "Klaava";
	            break;
	        default:
	            choice = null;
	    }

	    return choice;
	}

    public String computerCoin() {

	    // Create a Random object.
	    Random rand = new Random();
	
	    // Generate a random number in the range of 1 to 2.
	    int num = rand.nextInt(2) + 1;
	    computerCoin = num;
	    // Return computer's coin.
	    return getCoin(num) ;
	}
    
    public String userCoin() {
    	
    	 // Ask the user for input
    	 System.out.print("Enter 1 - Kruunu or 2 - Klaava: \n");
    	 int userCoin = sc.nextInt();    	 	 
    	 String coin = getCoin(userCoin);

    	// Validate the choice.
    	while (coin == null) {
    	   System.out.print("Enter 1 - Kruunu or 2 - Klaava: \n");
    	   userCoin = sc.nextInt();
    	   coin = getCoin(userCoin);
    	}
    	
    	
    	playerCoin = userCoin;

    	return coin;
    }

    public String printCoins(String computerCoin, String userCoin) {
    	String output;

        output = "The computer's coin was " + computerCoin + ".\n";
        output += "Your coin was " + userCoin + ".\n\n";
        
        if (userCoin == computerCoin)
        	output += "You and the computer have same coins.\n";
		 else {
			output +="Your coin is different.\n";
		 }
        
        return output;
    }
    
    public void playAgain() {
        System.out.println("Play again?");
        System.out.println("Yes(Y) or No(N)");
        sc.nextLine();
        String ans = sc.nextLine();
        if (ans.equalsIgnoreCase("n")) {
        	System.out.println("The end.");
        	endOfGame = false;
        }
    }

}
