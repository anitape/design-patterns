package templatemethod;

abstract class Game {
	
	protected int playersCount;
    abstract void initializeGame();
    abstract void makePlay(int player);
    abstract boolean endOfGame();
    abstract void printWinner();
    
    public final void playGame(int playersCount) {
    	this.playersCount = playersCount; 
    	initializeGame();
    	while (endOfGame()){
            makePlay(playersCount);
            playersCount++;
            printWinner();
        }
    }
}
