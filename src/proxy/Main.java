package proxy;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	/**
	 * Test method
	 */
	public static void main(final String[] arguments) {

		Scanner sc = new Scanner(System.in);

		Image image1 = new ProxyImage("HiRes_10MB_Photo1");
		Image image2 = new ProxyImage("HiRes_10MB_Photo2");
		Image image3 = new ProxyImage("HiRes_10MB_Photo3");

		ArrayList<Image> imgFolder = new ArrayList<>();
		imgFolder.add(image1);
		imgFolder.add(image2);
		imgFolder.add(image3);
		
		System.out.println("Your image folder includes: ");
		for (Image image : imgFolder) {
			System.out.print(imgFolder.indexOf(image) + 1 + ": ");
			image.showData();
		}

		System.out.println("Load a new image: ");
		int img = sc.nextInt();		
		Image image = imgFolder.get(img-1);
		image.displayImage();
	}
}